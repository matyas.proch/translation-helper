## Set up Google API

1. https://console.cloud.google.com/apis/credentials
2. Set up API Key, OAuth consent screen and OAuth 2.0 Client ID


**OAuth consent screen**

- App name
- User support email
- Developer email address
- Scope: `Google Sheets API	.../auth/spreadsheets`
- Test users: your developers


**OAauth 2.0 Client ID:**

- Web application
- Name
- Authorized redirect URI: `http://localhost`

## Set up your TS Enum

```typescript
enum TranslationEnum {
  PROFILE_PICTURE = 'BACKEND.TEST3', // Comment
  TEAM_PROFILE_PICTURE = 'BACKEND.TEST4', // Comment
  PARTICIPANT_IMAGE = 'BACKEND.TEST5', // Comment
}
```

- The upload filters lines containing `=`
- It takes the key inside `''` and comment after `//`

## Set up your Google sheet

|Key|Comment|cs|en|es|ru|it|
|---|---|---|---|---|---|---|
|BACKEND.ERRORS.ACCOUNT_NOT_FOUND|During login|Účet nenalezen|Account not found||||

## Set up .env

```shell
TRANSLATION_GOOGLE_KEY=XY
TRANSLATION_GOOGLE_CLIENT_ID=XY
TRANSLATION_GOOGLE_SECRET=XY
TRANSLATION_GOOGLE_TOKEN_PATH=gtoken.json
TRANSLATION_SPREADSHEET_ID=XY
TRANSLATION_SHEET_NAME='List 1'
TRANSLATION_MODE=BACKEND
TRANSLATION_OUTPUT_FOLDER='./src/i18n'
TRANSLATION_INPUT_ENUM_PATH='./src/TranslationEnum.ts'
```

## Usage

```shell
npm i -g @mattproch-dev/translation-git

translation upload
translation download
```

**Upload**

- Upload only uploads the keys and comments
- Local comments overwrite the ones in gSheet
- Extra keys in gSheet are not deleted

**Download**

- Downloads all the translation in separate folders by languages in one big file `text.json`
