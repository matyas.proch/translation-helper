#!/usr/bin/env node

"use strict";
exports.__esModule = true;
var dotenv = require("dotenv");
var express = require("express");
var fs = require("fs");
var googleapis_1 = require("googleapis");
var port = 4444;
dotenv.config();
var config = {
    googleKey: process.env.TRANSLATION_GOOGLE_KEY,
    googleClientId: process.env.TRANSLATION_GOOGLE_CLIENT_ID,
    googleSecret: process.env.TRANSLATION_GOOGLE_SECRET,
    googleToken: process.env.TRANSLATION_GOOGLE_TOKEN_PATH,
    googleScopes: ['https://www.googleapis.com/auth/spreadsheets'],
    googleSpreadsheetId: process.env.TRANSLATION_SPREADSHEET_ID,
    googleSheetName: process.env.TRANSLATION_SHEET_NAME,
    mode: process.env.TRANSLATION_MODE,
    inputEnum: process.env.TRANSLATION_INPUT_ENUM_PATH,
    outputFolder: process.env.TRANSLATION_OUTPUT_FOLDER
};
var action = process.argv[2];
var loggedIn = function (auth) {
    var sheets = googleapis_1.google.sheets({ version: 'v4', auth: auth });
    var upload = function (data) {
        if (data.length === 0) {
            return;
        }
        var content = fs.readFileSync(config.inputEnum).toString();
        var lines = content.split('\n');
        var output = {};
        data.forEach(function (row) {
            var key = row[0];
            output[key] = row[1];
        });
        lines.forEach(function (line) {
            if (!line.includes('=')) {
                return;
            }
            var _a = line.split("'").map(function (part) { return part.trim(); }), key = _a[1], rawComment = _a[2];
            var _b = rawComment.split('//').map(function (part) { return part.trim(); }), comment = _b[1];
            output[key] = comment !== null && comment !== void 0 ? comment : '';
        });
        sheets.spreadsheets.values.update({
            key: config.googleKey,
            spreadsheetId: config.googleSpreadsheetId,
            range: config.googleSheetName,
            requestBody: {
                values: Object.entries(output)
            },
            valueInputOption: 'RAW'
        }, function (err) {
            if (err) {
                return console.log("The API returned an error: " + err);
            }
            console.log('Uploaded');
        });
    };
    var download = function (data) {
        if (data.length === 0) {
            return;
        }
        var set = function (object, keys, content) {
            keys.slice(0, keys.length - 1).forEach(function (key) {
                if (!(key in object)) {
                    object[key] = {};
                }
                object = object[key];
            });
            object[keys[keys.length - 1]] = content;
        };
        var languages = data[0].slice(2);
        var output = {};
        languages.forEach(function (language) {
            output[language] = {};
        });
        data.slice(1).forEach(function (row) {
            var key = row[0].split('.');
            row.slice(2).forEach(function (text, index) {
                set(output[languages[index]], key, text);
            });
        });
        languages.forEach(function (language) {
            if (!fs.existsSync(config.outputFolder + "/" + language)) {
                fs.mkdirSync(config.outputFolder + "/" + language);
            }
            if (fs.existsSync(config.outputFolder + "/" + language + "/text.json")) {
                fs.unlinkSync(config.outputFolder + "/" + language + "/text.json");
            }
            fs.writeFileSync(config.outputFolder + "/" + language + "/text.json", JSON.stringify(output[language], null, 2));
        });
        console.log('Downloaded');
    };
    var start = function (data) {
        switch (action) {
            case 'upload':
                upload(data);
                break;
            case 'download':
                download(data);
                break;
        }
    };
    sheets.spreadsheets.values.get({
        key: config.googleKey,
        spreadsheetId: config.googleSpreadsheetId,
        range: config.googleSheetName
    }, function (err, res) {
        var _a;
        if (err) {
            return console.log("The API returned an error: " + err);
        }
        var data = (_a = res.data.values) !== null && _a !== void 0 ? _a : [];
        start(data);
    });
};
function getNewToken(oAuth2Client, callback) {
    var app = express();
    var server = app.listen(port, function () { });
    app.get('/', function (req, res) {
        var code = req.query.code;
        if (code) {
            res.status(200).send(code);
            server.close();
            oAuth2Client.getToken(code, function (err, token) {
                if (err) {
                    return console.error('Error while trying to retrieve access token', err);
                }
                oAuth2Client.setCredentials(token);
                // Store the token to disk for later program executions
                fs.writeFile(config.googleToken, JSON.stringify(token), function (err2) {
                    if (err2) {
                        return console.error(err2);
                    }
                    console.log('Token stored to', config.googleToken);
                });
                callback(oAuth2Client);
            });
        }
        else {
            res.status(404).send("Missing code!");
        }
    });
    var authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: config.googleScopes
    });
    console.log('Authorize this app by visiting this url:', authUrl);
}
var oAuth2Client = new googleapis_1.google.auth.OAuth2(config.googleClientId, config.googleSecret, "http://localhost:" + port);
fs.readFile(config.googleToken, function (err, token) {
    if (err) {
        return getNewToken(oAuth2Client, loggedIn);
    }
    oAuth2Client.setCredentials(JSON.parse(token.toString()));
    loggedIn(oAuth2Client);
});
